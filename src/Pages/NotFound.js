import React, {PureComponent} from 'react';
import LogoHeading from 'Components/LogoHeading';

import {locale} from 'Config/locale';
import './NotFound.scss';

export default class NotFound extends PureComponent {
    render() {
        return (
            <div>
                <LogoHeading title={locale.notFound.title} />
                <div className="notFound">
                    <p>Not enough power to access this page.</p>

                    <p>Make sure you typed in the page address correctly or go back to your previous page.</p>
                    <img src="https://i.annihil.us/u/prod/marvel/html_blocks_assets/scarlet_witch_404/img/scarlet.png" alt="Page not found" />
                </div>
            </div>
        );
    }
}
