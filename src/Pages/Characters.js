import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import CSSTransition from 'react-transition-group/CSSTransition';

import {loadCharacters, loadMoreCharacters} from 'Actions/Characters';
import CharacterCard from 'Components/CharacterCard';
import ActionButton from 'Components/ActionButton';
import LogoHeading from 'Components/LogoHeading';
import LoadingAnimation from 'Components/LoadingAnimation';
import CustomSearch from 'Components/CustomSearch';
import NoSearchResults from 'Components/NoSearchResults';
import Footer from 'Components/Footer';
import {locale} from 'Config/locale';

import './Characters.scss';

class Characters extends PureComponent {
    static propTypes = {
        characters: PropTypes.object,
        loadCharacters: PropTypes.func,
        loadMoreCharacters: PropTypes.func,
        isLoading: PropTypes.bool,
        selectedCharacter: PropTypes.object,
        searchTerm: PropTypes.string
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {characters, loadCharacters} = this.props;
        if (Object.keys(characters.get('characters').toJS()).length === 0) {
            loadCharacters();
        }
    }

    canShowMoreCharacters = () => {
        const {characters} = this.props;
        const paginationData = characters.toJS().characters.data;
        if (!paginationData) return false;
        return (paginationData.offset + paginationData.count) < paginationData.total;
    };

    showMoreCharacters = () => {
        const {characters, searchTerm} = this.props;
        const paginationData = characters.toJS().characters.data;
        const offset = paginationData.offset + paginationData.count;

        this.props.loadMoreCharacters(offset, searchTerm);
    };

    render() {
        const {characters, isLoading, searchTerm} = this.props;
        const characterData = characters.get('characters');

        return (
            <div className="mdl-layout mdl-js-layout">
                <LogoHeading title={locale.logoHeading.title.characters}>
                    <CustomSearch type="character" />
                </LogoHeading>

                {isLoading && (
                    <LoadingAnimation />
                )}

                {!isLoading && searchTerm.length > 0 && characterData.getIn(['data', 'total']) === 0 && (
                    <NoSearchResults searchTerm={searchTerm} />
                )}
                <main className="mdl-layout__content">
                    <CSSTransition classNames="fade" timeout={locale.transition.timeout} in={!isLoading}>
                        <div className={`characters-grid mdl-grid ${isLoading ? 'loading' : ''}`}>
                            {characterData.size > 0 && (
                                characterData.get('data').get('results').map(character => {
                                    return (
                                        <CharacterCard
                                            key={character.get('id')}
                                            character={character} />
                                    );
                                })
                            )}
                            {this.canShowMoreCharacters() && (
                                <div className="buttonContainer mdl-cell mdl-cell--stretch">
                                    <ActionButton label={locale.characterList.buttons.more} handleClick={this.showMoreCharacters} />
                                </div>
                            )}
                        </div>
                    </CSSTransition>
                </main>
                <Footer fixedPosition={isLoading}>
                    {characterData.get('attributionText')}
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const characters = state.characters;
    return {
        characters: state.characters,
        isLoading: characters.get('isLoading'),
        selectedCharacter: characters.get('selectedCharacter'),
        searchTerm: characters.get('searchTerm')
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadCharacters: () => dispatch(loadCharacters()),
        loadMoreCharacters: (offset, searchTerm) => dispatch(loadMoreCharacters(offset, searchTerm))
    };
};

Characters = connect(
    mapStateToProps,
    mapDispatchToProps
)(Characters);

export default Characters;
