import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import CSSTransition from 'react-transition-group/CSSTransition';

import {locale} from 'Config/locale';
import ActionButton from 'Components/ActionButton';
import LoadingAnimation from 'Components/LoadingAnimation';
import InfoPanel from 'Components/InfoPanel';
import {updateToHttps} from 'Helpers/utils';
import './CharacterBio.scss';
import {clearSelectedCharacter, loadSelectedCharacter} from 'Actions/CharacterBio';

export class CharacterBio extends Component {
    static propTypes = {
        characters: PropTypes.object,
        clearSelectedCharacter: PropTypes.func,
        comicInfo: PropTypes.object,
        isLoading: PropTypes.bool,
        loadSelectedCharacter: PropTypes.func,
        match: PropTypes.object,
        selectedCharacter: PropTypes.object
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {loadSelectedCharacter, match} = this.props;

        if (this.checkCharacterListLoaded()) {
            loadSelectedCharacter(match.params.id);
        }
    }

    componentWillUnmount() {
        this.props.clearSelectedCharacter();
    }

    checkCharacterListLoaded = () => {
        return Object.keys(this.props.characters.get('characters').toJS()).length > 0;
    };

    extractUrlData = (urls) => {
        const labels = locale.character.url.labels;

        if (urls) {
            return urls.map((data, i) => {
                return (
                    <div key={data.get('type')}>
                        <a href={data.get('url')} target={`_${data.get('type')}`}>{labels[i][data.get('type')]}</a>
                    </div>
                );
            });
        }

        return null;
    };

    render() {
        if (!this.checkCharacterListLoaded()) {
            return (<Redirect to="/characters" />);
        }
        const {isLoading, selectedCharacter} = this.props;
        const bioData = selectedCharacter.get('bio');
        const https = updateToHttps(bioData.getIn(['thumbnail', 'path']));

        const InfoPanelSource = {
            comics: selectedCharacter.get('comics'),
            events: selectedCharacter.get('events'),
            series: selectedCharacter.get('series'),
            stories: selectedCharacter.get('stories')
        };

        return (
            <div className="mdl-layout mdl-js-layout">
                <main className="mdl-layout__content">
                    {isLoading && (
                        <LoadingAnimation />
                    )}
                    <CSSTransition classNames="fade" timeout={locale.transition.timeout} in={!isLoading}>
                        <div className="character-bio mdl-grid">
                            <div className="character mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                {!isLoading && (
                                    <picture>
                                        <source srcSet={`${https}/standard_fantastic.${bioData.getIn(['thumbnail', 'extension'])}`} media="(max-width: 2100px)" />
                                        <source srcSet={`${https}/detail.${bioData.getIn(['thumbnail', 'extension'])}`} media="(min-width: 2100px)" />
                                        <img srcSet="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Placeholder" />
                                    </picture>
                                )}
                                <div className="name">{bioData.get('name')}</div>
                                <div className="description">{bioData.get('description')}</div>
                                <div className="mdl-grid">
                                    <div className="mdl-cell mdl-cell--9-col">
                                        {this.extractUrlData(bioData.get('urls'))}
                                    </div>
                                    <div className="mdl-cell mdl-cell--3-col">
                                        {!isLoading && (
                                            <ActionButton label="Close" linkTo="/characters" size="smallLink" />
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="content mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                {Object.keys(InfoPanelSource).map(key => {
                                    if (InfoPanelSource[key].size === 0) return null;
                                    return (
                                        <div key={key} className="mdl-tabs__panel" id={`${key}-panel`}>
                                        <InfoPanel title={key} data={InfoPanelSource[key]} />
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </CSSTransition>
                </main>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const characters = state.characters;
    return {
        characters,
        isLoading: characters.get('isLoading'),
        selectedCharacter: characters.get('selectedCharacter')
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clearSelectedCharacter: () => dispatch(clearSelectedCharacter()),
        loadSelectedCharacter: (id) => dispatch(loadSelectedCharacter(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CharacterBio);
