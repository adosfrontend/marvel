import React, {PureComponent} from 'react';
import YouTube from 'react-youtube';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';

import ActionButton from 'Components/ActionButton';
import LoadingAnimation from 'Components/LoadingAnimation';
import {locale} from 'Config/locale';
import './Intro.scss';

class Intro extends PureComponent {
    static propTypes = {
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            showLoadingAnimation: true
        };
    }

    hideLoadingAnimation = () => {
        this.setState({showLoadingAnimation: false});
    };

    showCharacters = () => {
        this.props.history.push('/characters');
    };

    render() {
        const opts = {
            playerVars: {
                autoplay: 1,
                showinfo: 0,
                controls: 0
            }
        };
        return (
            <div>
                {this.state.showLoadingAnimation && (
                    <LoadingAnimation />
                )}

                <YouTube
                    playsinline={0}
                    videoId="hvha-7EvwNg"
                    id="marvelPlayer"
                    opts={opts}
                    onReady={this.hideLoadingAnimation}
                    onEnd={this.showCharacters}
                />

                {!this.state.showLoadingAnimation && (
                    <div className="introBtnContainer">
                        <ActionButton
                            label={locale.intro.buttons.skip}
                            handleClick={this.showCharacters}
                            size="smallBtn semiVisible"
                        />
                    </div>
                )}
            </div>
        );
    }
}

Intro = withRouter(Intro);

export default Intro;
