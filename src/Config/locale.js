export const locale = {
    characterList: {
        buttons: {
            more: 'Show more characters!'
        }
    },
    character: {
        url: {
            labels: [
                {detail: 'View more details about this character'},
                {wiki: 'Read more on the Marvel Universe Wiki'},
                {comiclink: 'View more comics'}
            ]
        },
    },
    intro: {
        buttons: {
            skip: 'Skip Intro'
        }
    },
    logoHeading: {
        title: {
            characters: 'Characters'
        }
    },
    noSearchResults: {
        message: 'Your search for "[searchTerm]" has returned zero results.'
    },
    notFound: {
        title: 'Oops!'
    },
    searchBox: {
        label: [
            {character: 'Search for a character ...'}
        ],
        buttons: {
            clear: 'Clear'
        },
        noCharacters: 0,
        timeout: 300
    },
    transition: {
        timeout: 300
    }
};
