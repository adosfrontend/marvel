import {API_KEY, api} from 'Helpers/xhr';

// Types
export const CHARACTERS_LOADING = 'CHARACTERS_LOADING';
export const CHARACTERS_LOADED = 'CHARACTERS_LOADED';
export const CHARACTER_LOADING_FAILED = 'CHARACTER_LOADING_FAILED';
export const CHARACTERS_OFFSET_LOADED = 'CHARACTERS_OFFSET_LOADED';

/**
 * Retrieve the character data
 * @returns {function(*)}
 */
export function loadCharacters() {
    return dispatch => {
        dispatch({type: CHARACTERS_LOADING});
        api.get(`characters?${API_KEY}`).then(response => {
            dispatch({
                type: CHARACTERS_LOADED,
                payload: response.data
            });
        }).catch(() => {
            dispatch({
                type: CHARACTER_LOADING_FAILED
            });
        });
    };
}

function getSearchEndpoint(offset, searchTerm) {
    if(searchTerm.length > 0) {
        return `characters?offset=${offset}&nameStartsWith=${searchTerm}&${API_KEY}`;
    }

    return `characters?offset=${offset}&${API_KEY}`;
}

export function loadMoreCharacters(offset, searchTerm) {
    return dispatch => {
        dispatch({type: CHARACTERS_LOADING});
        api.get(getSearchEndpoint(offset, searchTerm)).then(response => {
            dispatch({
                type: CHARACTERS_OFFSET_LOADED,
                payload: response.data
            });
        }).catch(() => {
            dispatch({
                type: CHARACTER_LOADING_FAILED
            });
        });
    };
}

