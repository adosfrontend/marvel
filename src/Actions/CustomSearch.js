import {API_KEY, api} from 'Helpers/xhr';

// Types
export const CUSTOM_SEARCH_STARTED = 'CUSTOM_SEARCH_STARTED';
export const CUSTOM_CHARACTER_SEARCH_COMPLETE = 'CUSTOM_CHARACTER_SEARCH_COMPLETE';
export const CUSTOM_SEARCH_FAILED = 'CUSTOM_SEARCH_FAILED';
export const SEARCH_TERM_CLEARED = 'SEARCH_TERM_CLEARED';

function getSearchEndpoint(searchTerm) {
    if(searchTerm.length > 0) {
        return `characters?nameStartsWith=${searchTerm}&${API_KEY}`;
    }

    return `characters?${API_KEY}`;
}

/**
 * Search for a particular character based on the partialName provided
 * @returns {function(*)}
 */
export function searchForCharacter(searchTerm) {
    return dispatch => {
        dispatch({type: CUSTOM_SEARCH_STARTED});
        api.get(getSearchEndpoint(searchTerm)).then(response => {
            dispatch({
                type: CUSTOM_CHARACTER_SEARCH_COMPLETE,
                payload: {
                    data: response.data,
                    searchTerm: searchTerm
                }
            });
        }).catch(() => {
            dispatch({
                type: CUSTOM_SEARCH_FAILED
            });
        });
    };
}

export function clearSearchTerm() {
    return {
        type: SEARCH_TERM_CLEARED
    };
}
