import {API_KEY, api} from 'Helpers/xhr';

// Types
export const CHARACTER_DATA_LOADED = 'CHARACTER_DATA_LOADED';
export const CHARACTER_DATA_LOADING = 'CHARACTER_DATA_LOADING';
export const CHARACTER_DATA_LOADING_FAILED = 'CHARACTER_DATA_LOADING_FAILED';
export const CHARACTER_SELECTION_RESET = 'CHARACTER_SELECTION_RESET';

/**
 * Retrieve the character data
 * @returns {function(*)}
 */
function loadCharacterData(characterId, endpoint) {
    return api.get(`characters/${characterId}/${endpoint}?${API_KEY}`);
}

export function loadSelectedCharacter(characterId) {
    return dispatch => {
        dispatch({type: CHARACTER_DATA_LOADING});
        Promise.all([loadCharacterData(characterId, 'comics'), loadCharacterData(characterId, 'events'), loadCharacterData(characterId, 'series'), loadCharacterData(characterId, 'stories')]).then(([comics, events, series, stories]) => {
            dispatch({
                type: CHARACTER_DATA_LOADED,
                payload: {
                    characterId,
                    comics: comics.data.data.results,
                    events: events.data.data.results,
                    series: series.data.data.results,
                    stories: stories.data.data.results
                }
            });
        }).catch(() => {
            dispatch({
                type: CHARACTER_DATA_LOADING_FAILED
            });
        });
    };
}

export function clearSelectedCharacter() {
    return {
        type: CHARACTER_SELECTION_RESET
    };
}
