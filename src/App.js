import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Characters from 'Pages/Characters';
import CharacterBio from 'Pages/CharacterBio';
import Intro from 'Pages/Intro';
import NotFound from 'Pages/NotFound';

import './App.scss';

const App = () => (
    <div>
        <Switch>
            <Route exact path="/" component={Intro}/>
            <Route path="/characters" component={Characters}/>
            <Route path="/character/bio/:id" component={CharacterBio}/>
            <Route path="*" component={NotFound} />
        </Switch>
    </div>
);

export default App;
