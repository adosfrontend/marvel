import {combineReducers} from 'redux';

// Load reducers
import CharactersReducer from './Reducers/Characters';

export const rootReducer = combineReducers({
    characters: CharactersReducer
});
