import Immutable from 'immutable';

import CharactersReducer, {initialState} from 'Reducers/Characters';
import {characters} from './data/Characters';
import {
    CHARACTERS_LOADING,
    CHARACTERS_LOADED,
    CHARACTER_LOADING_FAILED,
    CHARACTERS_OFFSET_LOADED
} from 'Actions/Characters';

import {
    CUSTOM_SEARCH_STARTED,
    CUSTOM_CHARACTER_SEARCH_COMPLETE,
    CUSTOM_SEARCH_FAILED,
    SEARCH_TERM_CLEARED
} from 'Actions/CustomSearch';

import {
    CHARACTER_DATA_LOADED,
    CHARACTER_DATA_LOADING,
    CHARACTER_DATA_LOADING_FAILED,
    CHARACTER_SELECTION_RESET
} from 'Actions/CharacterBio';

function setTestObject(dirtyObj) {
    return Object.assign({}, initialState.toJS(), dirtyObj);
}

describe('Characters reducer tests', () => {
    it('should return the initial state', () => {
        expect(CharactersReducer(undefined, {})).toEqual(initialState);
    });

    it('should set the isLoading state to true', () => {
        const testObj = Immutable.fromJS(setTestObject({isLoading: false}));
        expect(
            CharactersReducer(testObj, {
                type: CHARACTER_DATA_LOADING
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: true})
            )
        );

        expect(
            CharactersReducer(testObj, {
                type: CHARACTERS_LOADING
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: true})
            )
        );

        expect(
            CharactersReducer(testObj, {
                type: CUSTOM_SEARCH_STARTED
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: true})
            )
        );
    });

    it('should set the isLoading state to true', () => {
        const testObj = Immutable.fromJS(setTestObject({isLoading: false}));

        expect(
            CharactersReducer(testObj, {
                type: CHARACTERS_LOADING
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: true})
            )
        );
    });

    it('should set the characters are loaded', () => {
        const testObj = Immutable.fromJS(setTestObject({isLoading: true}));

        expect(
            CharactersReducer(testObj, {
                type: CHARACTERS_LOADED,
                payload: characters.get('characters')
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({
                    characters: characters.get('characters'),
                    isLoading: false
                })
            )
        );
    });

    it('should set the offset characters are loaded', () => {
        const testObj = Immutable.fromJS(setTestObject({
            characters: characters.get('characters'),
            isLoading: true}));

        const combinedCharacters = characters.getIn(['characters', 'data', 'results']).toJS().concat(characters.getIn(['characters', 'data', 'results']).toJS());
        const expectedData = characters.setIn(['characters', 'data', 'results'], Immutable.fromJS(combinedCharacters)).get('characters');

        expect(
            CharactersReducer(testObj, {
                type: CHARACTERS_OFFSET_LOADED,
                payload: characters.get('characters').toJS()
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({
                    characters: expectedData,
                    isLoading: false
                })
            )
        );
    });

    it('should set the search term state and character data', () => {
        const testObj = Immutable.fromJS(setTestObject({isLoading: true}));

        expect(
            CharactersReducer(testObj, {
                type: CUSTOM_CHARACTER_SEARCH_COMPLETE,
                payload: {
                    data: characters.get('characters'),
                    searchTerm: 'spider'
                }
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({
                    characters: characters.get('characters'),
                    searchTerm: 'spider',
                    isLoading: false
                })
            )
        );
    });

    it('should reset the isLoading state to false', () => {
        const testObj = Immutable.fromJS(setTestObject({isLoading: true}));
        expect(
            CharactersReducer(testObj, {
                type: CHARACTER_LOADING_FAILED
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: false})
            )
        );

        expect(
            CharactersReducer(testObj, {
                type: CHARACTER_DATA_LOADING_FAILED
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: false})
            )
        );

        expect(
            CharactersReducer(testObj, {
                type: CUSTOM_SEARCH_FAILED
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({isLoading: false})
            )
        );
    });

    it('should clear the selected character', () => {
        const testObj = Immutable.fromJS(setTestObject({
            selectedCharacter: {
                bio: characters.getIn(['characters', 'data', 'results']).toJS(),
                comics: {resultsData: 'some data'},
                events: {resultsData: 'some data'},
                series: {resultsData: 'some data'},
                stories: {resultsData: 'some data'}
            },
            isLoading: true
        }));

        expect(
            CharactersReducer(testObj, {
                type: CHARACTER_SELECTION_RESET
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({
                    selectedCharacter: initialState.get('selectedCharacter'),
                    isLoading: false
                })
            )
        );
    });

    it('should load the character data', () => {
        const testObj = Immutable.fromJS(setTestObject({'characters': characters.get('characters')}));
        const expectedData = Immutable.fromJS(setTestObject({
            characters: characters.get('characters').toJS(),
            selectedCharacter: {
                bio: characters.getIn(['characters', 'data', 'results']).get(0).toJS()
            },
            isLoading: false
        }));

        const combinedExpectedData = expectedData.setIn(['selectedCharacter', 'comics'], {resultsData: 'some data'})
            .setIn(['selectedCharacter', 'events'], {resultsData: 'some data'})
            .setIn(['selectedCharacter', 'series'], {resultsData: 'some data'})
            .setIn(['selectedCharacter', 'stories'], {resultsData: 'some data'});

        expect(
            CharactersReducer(testObj, {
                type: CHARACTER_DATA_LOADED,
                payload: {
                    characterId: 1011334,
                    comics: {resultsData: 'some data'},
                    events: {resultsData: 'some data'},
                    series: {resultsData: 'some data'},
                    stories: {resultsData: 'some data'}
                }
            })
        ).toEqual(
            combinedExpectedData
        );
    });

    it('should clear the search term', () => {
        expect(
            CharactersReducer(initialState, {
                type: SEARCH_TERM_CLEARED
            })
        ).toEqual(
            Immutable.fromJS(
                setTestObject({searchTerm: ''})
            )
        );
    });
});
