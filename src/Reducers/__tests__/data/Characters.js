import Immutable from 'immutable';

export const characters = Immutable.fromJS(
    {
        'characters': {
            'code': 200,
            'status': 'Ok',
            'copyright': '© 2017 MARVEL',
            'attributionText': 'Data provided by Marvel. © 2017 MARVEL',
            'attributionHTML': '<a href=\'http://marvel.com\'>Data provided by Marvel. © 2017 MARVEL</a>',
            'etag': '4141a886df2246abbf541eb1e50ff938f2143a68',
            'data': {
                'offset': 0,
                'limit': 20,
                'total': 1485,
                'count': 20,
                'results': [
                    {
                        'urls': [
                            {
                                'type': 'detail',
                                'url': 'http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=71cc40c6165f2ecfb4dd06354051f5f8'
                            },
                            {
                                'type': 'wiki',
                                'url': 'http://marvel.com/universe/3-D_Man_(Chandler)?utm_campaign=apiRef&utm_source=71cc40c6165f2ecfb4dd06354051f5f8'
                            },
                            {
                                'type': 'comiclink',
                                'url': 'http://marvel.com/comics/characters/1011334/3-d_man?utm_campaign=apiRef&utm_source=71cc40c6165f2ecfb4dd06354051f5f8'
                            }
                        ],
                        'modified': '2014-04-29T14:18:17-0400',
                        'name': '3-D Man',
                        'thumbnail': {
                            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
                            'extension': 'jpg'
                        },
                        'id': 1011334,
                        'description': '',
                        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1011334'
                    }
                ]
            }
        },
        'isLoading': false,
        'searchTerm': ''
    }
);
