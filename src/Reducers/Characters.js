import Immutable from 'immutable';

import {
    CHARACTERS_LOADING,
    CHARACTERS_LOADED,
    CHARACTER_LOADING_FAILED,
    CHARACTERS_OFFSET_LOADED
} from 'Actions/Characters';

import {
    CUSTOM_SEARCH_STARTED,
    CUSTOM_CHARACTER_SEARCH_COMPLETE,
    CUSTOM_SEARCH_FAILED,
    SEARCH_TERM_CLEARED
} from 'Actions/CustomSearch';

import {
    CHARACTER_DATA_LOADED,
    CHARACTER_DATA_LOADING,
    CHARACTER_DATA_LOADING_FAILED,
    CHARACTER_SELECTION_RESET
} from 'Actions/CharacterBio';

// Immutable state
export const initialState = Immutable.fromJS({
    characters: {},
    selectedCharacter: {
        bio: {},
        comics: [],
        events: [],
        series: [],
        stories: []
    },
    isLoading: false,
    searchTerm: ''
});

const CharactersReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHARACTER_DATA_LOADING:
        case CHARACTERS_LOADING:
        case CUSTOM_SEARCH_STARTED:
            return state.set('isLoading', true);
        case CHARACTERS_LOADED:
            return state.set('characters', Immutable.fromJS(action.payload)).set('isLoading', false);
        case CUSTOM_CHARACTER_SEARCH_COMPLETE:
            return state.set('characters', Immutable.fromJS(action.payload.data)).set('searchTerm', Immutable.fromJS(action.payload.searchTerm)).set('isLoading', false);
        case CHARACTER_LOADING_FAILED:
        case CHARACTER_DATA_LOADING_FAILED:
        case CUSTOM_SEARCH_FAILED:
            return state.set('isLoading', false);
        case CHARACTERS_OFFSET_LOADED:
            const characters = action.payload;
            const combinedCharacters = state.getIn(['characters', 'data', 'results']).toJS().concat(characters.data.results);
            characters.data.results = combinedCharacters;
            return state.set('characters', Immutable.fromJS(action.payload)).set('isLoading', false);
        case CHARACTER_SELECTION_RESET:
            return state.set('selectedCharacter', initialState.get('selectedCharacter')).set('isLoading', false);
        case CHARACTER_DATA_LOADED:
            const {characterId, comics, events, series, stories} = action.payload;
            const chosenCharacter = state.getIn(['characters', 'data', 'results']).filter(character =>  character.get('id') === Number(characterId));

            return state
                .setIn(['selectedCharacter', 'bio'], chosenCharacter.get(0))
                .setIn(['selectedCharacter', 'comics'], comics)
                .setIn(['selectedCharacter', 'events'], events)
                .setIn(['selectedCharacter', 'series'], series)
                .setIn(['selectedCharacter', 'stories'], stories)
                .set('isLoading', false);
        case SEARCH_TERM_CLEARED:
            return state.set('searchTerm', initialState.get('searchTerm'));
        default:
            return state;
    }
};

export default CharactersReducer;
