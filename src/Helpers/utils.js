export function updateToHttps(protocol) {
    if (!protocol) return '';
    return protocol.replace('http', 'https');
}
