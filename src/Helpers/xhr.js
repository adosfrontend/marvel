import axios from 'axios';

export const API_KEY = 'apikey=71cc40c6165f2ecfb4dd06354051f5f8';

export const api = axios.create({
    baseURL: 'https://gateway.marvel.com:443/v1/public/'
});
