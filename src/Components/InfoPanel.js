import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import {updateToHttps} from 'Helpers/utils';
import './InfoPanel.scss';

class InfoPanel extends PureComponent {
    static propTypes = {
        data: PropTypes.array.isRequired,
        title: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {data, title} = this.props;
        return (
            <div>
                {data.length > 0 && (
                    <div className="infoPanel mdl-grid">
                        <div className="mdl-cell mdl-cell--12-col">
                            <h2>{title}</h2>
                        </div>
                        <div className="mdl-cell mdl-cell--12-col">
                            <div className="infoCell mdl-grid">
                                {data.map((d, i) => {
                                    const panelId = `tt${i}`;
                                    return (
                                        <div key={`${title}${d.title}${i}`} className="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet">
                                            {d.thumbnail && (
                                            <img
                                                src={`${updateToHttps(d.thumbnail.path)}/portrait_xlarge.${d.thumbnail.extension}`}
                                                alt={d.title}/>
                                            )}
                                            <div className="title" id={panelId}>{d.title}</div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default InfoPanel;
