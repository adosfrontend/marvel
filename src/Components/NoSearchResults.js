import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import {locale} from 'Config/locale';
import './NoSearchResults.scss';

class NoSearchResults extends PureComponent {
    static propTypes = {
        searchTerm: PropTypes.string
    };

    constructor(props) {
        super(props);
    }

    render() {
        const msg = locale.noSearchResults.message.replace('[searchTerm]', this.props.searchTerm);

        return (
            <div className="noSearchResults">
                <div className="content">{msg}</div>
                <img src="https://i.annihil.us/u/prod/marvel/html_blocks_assets/captain_america_404/img/cap404.png" alt="Captain America" />
            </div>
        );
    }
}

export default NoSearchResults;
