import React, {PureComponent} from 'react';

import './LoadingAnimation.scss';

class LoadingAnimation extends PureComponent {
    render() {
        return (
            <div className="preloader loading">
                <span className="slice" />
                <span className="slice" />
                <span className="slice" />
                <span className="slice" />
                <span className="slice" />
                <span className="slice" />
            </div>
        );
    }
}

export default LoadingAnimation;
