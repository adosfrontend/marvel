jest.dontMock('Components/InfoPanel');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {infoPanelTestData} from './data/InfoPanel';

import InfoPanel from 'Components/InfoPanel';

function setup(opts = {}) {
    const props = {
        title: 'Characters',
        data: infoPanelTestData
    };

    const combinedProps = Object.assign({}, props, opts);

    return shallow(<InfoPanel {...combinedProps} />);
}

describe('InfoPanel tests', () => {
    it('renders a default InfoPanel component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });
});
