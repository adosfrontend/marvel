jest.dontMock('Components/CharacterCard');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import {CharacterCard} from 'Components/CharacterCard';
import {character} from './data/CharacterCard';

function setup(opts = {}) {
    const props = {
        character
    };

    const combinedProps = Object.assign({}, props, opts);

    return shallow(<CharacterCard {...combinedProps} />);
}

describe('CharacterCard tests', () => {
    it('renders a default CharacterCard component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });

    it('renders a default CharacterCard component during loading', () => {
        const app = setup({
            isLoading: true
        });
        expect(toJson(app)).toMatchSnapshot();
    });
});
