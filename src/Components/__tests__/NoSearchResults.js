jest.dontMock('Components/NoSearchResults');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import NoSearchResults from 'Components/NoSearchResults';

function setup() {
    const props = {
        searchTerm: 'spider'
    };
    return shallow(<NoSearchResults {...props} />);
}

describe('NoSearchResults tests', () => {
    it('renders a default NoSearchResults component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });
});
