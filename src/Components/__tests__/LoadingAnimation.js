jest.dontMock('Components/LoadingAnimation');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import LoadingAnimation from 'Components/LoadingAnimation';

function setup() {
    return shallow(<LoadingAnimation />);
}

describe('LoadingAnimation tests', () => {
    it('renders a default LoadingAnimation component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });
});
