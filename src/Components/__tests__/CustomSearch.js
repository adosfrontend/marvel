jest.dontMock('Components/CustomSearch');

import React from 'react';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import sinon from 'sinon';

import {CustomSearch} from 'Components/CustomSearch';

let clearSearchTerm;
let searchForCharacter;
let app;

describe('CustomSearch tests', () => {
    beforeEach(() => {
        clearSearchTerm = sinon.spy();
        searchForCharacter = sinon.spy();

        const props = {
            type: 'character',
            searchTerm: 'spider',
            clearSearchTerm,
            searchForCharacter
        };

        app =  mount(<CustomSearch {...props} />);
    });

    it('renders a default CustomSearch component', () => {
        expect(toJson(app)).toMatchSnapshot();
        expect(clearSearchTerm.calledOnce).toEqual(true);
    });

    it('checks the searchForCharacter function is called', () => {
        app.find('#searchBox').simulate('keyUp', { key: 'S' }).simulate('keyUp', { key: 'p' });
        setTimeout(() => expect(searchForCharacter.calledOnce).toEqual(true), 500);
    });

    it('checks the clearSearchResults function is called', () => {
        app.instance().clearSearchResults();
        expect(clearSearchTerm.calledTwice).toEqual(true);
        expect(searchForCharacter.calledOnce).toEqual(true);
    });
});
