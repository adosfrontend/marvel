jest.dontMock('Components/ActionButton');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import ActionButton from 'Components/ActionButton';

function setup(opts = {}) {
    const props = {
        label: 'test button '
    };

    const combinedProps = Object.assign({}, props, opts);

    return shallow(<ActionButton {...combinedProps} />);
}

describe('ActionButton tests', () => {
    it('renders a null ActionButton component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });

    it('renders the button ActionButton component', () => {
        const app = setup({
            handleClick: jest.fn
        });
        expect(toJson(app)).toMatchSnapshot();
    });

    it('renders link ActionButton component', () => {
        const app = setup({
            linkTo: 'someplace'
        });
        expect(toJson(app)).toMatchSnapshot();
    });
});
