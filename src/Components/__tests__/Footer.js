jest.dontMock('Components/Footer');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {footerTestData} from './data/Footer';

import Footer from 'Components/Footer';

function setup(opts = {}) {
    const props = {};

    const combinedProps = Object.assign({}, props, opts);

    return shallow(<Footer {...combinedProps}>{footerTestData}</Footer>);
}

describe('Footer tests', () => {
    it('renders a default Footer component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });

    it('renders a default Footer component with the fixed class', () => {
        const app = setup({
            fixedPosition: true
        });
        expect(toJson(app)).toMatchSnapshot();
    });
});
