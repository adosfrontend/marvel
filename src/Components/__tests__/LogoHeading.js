jest.dontMock('Components/LogoHeading');

import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import LogoHeading from 'Components/LogoHeading';

function setup(opts = {}) {
    const props = {
        title: 'test title'
    };

    const combinedProps = Object.assign({}, props, opts);

    return shallow(<LogoHeading {...combinedProps}>{opts.children}</LogoHeading>);
}

describe('LogoHeading tests', () => {
    it('renders a default LogoHeading component', () => {
        const app = setup();
        expect(toJson(app)).toMatchSnapshot();
    });

    it('renders a LogoHeading component with kids', () => {
        const app = setup({
            children: (
                <div>
                    some test children
                </div>
            )
        });
        expect(toJson(app)).toMatchSnapshot();
    });
});
