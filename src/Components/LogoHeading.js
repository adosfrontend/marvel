import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import './LogoHeading.scss';

class LogoHeading extends PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        children: PropTypes.any
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {children, title} = this.props;

        return (
        <header className="mdl-layout__header mdl-layout__header--transparent">
            <div className="mdl-layout__header-row">
                <Link to="/"
                      className="logo-heading">
                    {title}
                </Link>
            </div>
            {children && (
            <div className="mdl-layout__header-row">
                {children}
            </div>
            )}
        </header>
        );
    }
}

export default LogoHeading;
