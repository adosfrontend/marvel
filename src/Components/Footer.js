import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import './Footer.scss';

class Footer extends PureComponent {
    static propTypes = {
        children: PropTypes.any,
        fixedPosition: PropTypes.bool
    };

    static defaultProps = {
        fixedPosition: false
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {children, fixedPosition} = this.props;
        const fixedClass = fixedPosition ? 'fixed' : '';

        return (
        <footer className={fixedClass}>
            {children}
        </footer>
        );
    }
}

export default Footer;
