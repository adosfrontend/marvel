import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import './ActionButton.scss';

class ActionButton extends PureComponent {
    static propTypes = {
        disabled: PropTypes.bool,
        handleClick: PropTypes.func,
        label: PropTypes.string.isRequired,
        linkTo: PropTypes.string,
        size: PropTypes.string
    };

    static defaultProps = {
        handleClick: null,
        linkTo: null,
        size: ''
    };

    constructor(props) {
        super(props);
    }

    getButton() {
        const {label, handleClick, size, disabled} = this.props;
        return (
            <button className={`actionBtn ${size}`} onClick={handleClick} disabled={disabled}>
                {label}
            </button>
        );
    }

    getLink() {
        const {label, linkTo, size} = this.props;
        return (
            <Link to={linkTo} className={`actionBtn ${size}`}>
                {label}
            </Link>
        );
    }

    render() {
        const {handleClick, linkTo} = this.props;
        if (linkTo) {
            return  this.getLink();
        }

        if (handleClick) {
            return this.getButton();
        }

        return null;
    }
}

export default ActionButton;
