import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {searchForCharacter, clearSearchTerm} from 'Actions/CustomSearch';
import {locale} from 'Config/locale';
import ActionButton from 'Components/ActionButton';
import './CustomSearch.scss';

export class CustomSearch extends PureComponent {
    static propTypes = {
        clearSearchTerm: PropTypes.func,
        searchForCharacter: PropTypes.func,
        searchTerm: PropTypes.string,
        type: PropTypes.oneOf(['character', 'comics', 'creators', 'events', 'series', 'search']).isRequired
    };

    constructor(props) {
        super(props);

        this.timeoutId = null;
    }

    componentDidMount() {
        this.props.clearSearchTerm();
    }

    clearSearchResults = () => {
        const {clearSearchTerm, searchForCharacter} = this.props;
        this.textInput.value = '';
        clearSearchTerm();
        searchForCharacter('');
    };

    searchForCharacter = (e) => {
        const {searchForCharacter} = this.props;
        const searchTerm = e.target.value;

        if (this.timeoutId) {
            clearInterval(this.timeoutId);
        }

        this.timeoutId = setTimeout(() => searchForCharacter(searchTerm), locale.searchBox.timeout);
    };

    render() {
        const {searchTerm} = this.props;
        const searchBox = locale.searchBox;

        return (
            <div className="custom-search mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input className="mdl-textfield__input" type="text" ref={(input) => { this.textInput = input; }} id="searchBox" onKeyUp={e => this.searchForCharacter(e)} />
                <label className="mdl-textfield__label" htmlFor="searchBox">{searchBox.label[0][this.props.type]}</label>
                <ActionButton
                    label={searchBox.buttons.clear}
                    handleClick={this.clearSearchResults}
                    size="smallBtn"
                    disabled={searchTerm.length === locale.searchBox.noCharacters} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        searchTerm: state.characters.get('searchTerm')
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clearSearchTerm: () => dispatch(clearSearchTerm()),
        searchForCharacter: (partialName) => dispatch(searchForCharacter(partialName))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomSearch);
