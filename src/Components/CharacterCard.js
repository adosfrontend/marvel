import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {updateToHttps} from 'Helpers/utils';
import './CharacterCard.scss';

export class CharacterCard extends PureComponent {
    static propTypes = {
        character: PropTypes.object.isRequired,
        isLoading: PropTypes.bool
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {character, isLoading} = this.props;
        const thumbnail = character.get('thumbnail');
        const https = updateToHttps(thumbnail.get('path'));
        const url = `character/bio/${character.get('id')}`;

        return (
            <div className="character-card mdl-card mdl-shadow--2dp mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--6-col-phone">
                <div className="mdl-card_title mdl-card--expand">
                    {!isLoading && (
                        <Link to={url}
                              className="mdl-button mdl-js-button mdl-js-ripple-effect">
                            <picture>
                                <source srcSet={`${https}/portrait_incredible.${thumbnail.get('extension')}`} media="(max-width: 1199px)" />
                                <source srcSet={`${https}/portrait_uncanny.${thumbnail.get('extension')}`} media="(min-width: 1200px)" />
                                <img srcSet="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Placeholder" />
                            </picture>
                        </Link>
                    )}
                    {isLoading && (
                        <img src={`${https}/portrait_uncanny.${thumbnail.get('extension')}`} alt={character.get('name')} />
                    )}
                </div>
                <div className="mdl-card__actions">
                    {!isLoading && (
                    <Link to={url}
                          className="mdl-button mdl-js-button mdl-js-ripple-effect characterName">
                        {character.get('name')}
                    </Link>
                    )}
                    {isLoading && (
                        <div className="characterName">
                            {character.get('name')}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.characters.get('isLoading')
    };
};

export default connect(mapStateToProps)(CharacterCard);
