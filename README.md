# Marvel Application

For a local installation, first run `npm install`.

Then to run it locally, run `npm run start:dev`.

To view the app on Heroku, head to `https://marveladosfrontend.herokuapp.com/`

