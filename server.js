const path = require('path');
const express = require('express');
const app = express();

app.use(express.static(path.resolve('./dist/')));

app.get('*', (req, res) => res.sendFile((path.resolve('./dist/index.html'))));

app.listen(process.env.PORT || 3000, (err) => {
    if (err) {
        return console.error(err);
    }

    return null;
});
